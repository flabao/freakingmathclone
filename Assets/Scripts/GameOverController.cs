﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour {

    public Text currentScoreText;
    public Text highScoreText;

    public Button replayButton;

	// Use this for initialization
	void Start () {
        //check if the Unity editor sees an object assigned
        if (currentScoreText == null)
        {
            Debug.Log("Put a game object in current score text, please");
        }
        if (highScoreText == null)
        {
            Debug.Log("Put a game object in high score text, please");
        }

        //prints the current score from the Scores.cs script
        currentScoreText.GetComponent<Text>().text = "Current Score: " + Scores.currentScore.ToString();
        //gets the high score from player prefs
        highScoreText.GetComponent<Text>().text = "High Score: " + PlayerPrefs.GetInt("HIGH_SCORE", 0).ToString();
	}
	
	// Update is called once per frame
	void Update () {
        //escape key loads the menu scene
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MenuButtonClick();
        }
        //spacebar loads the game again
        if (Input.GetKeyDown(KeyCode.Space))
        {
            replayButton.onClick.Invoke();
        }
    }

    //load Game
    public void ReplayButtonClick()
    {
        SceneManager.LoadScene("GameScene");
    }

    //go to menu
    public void MenuButtonClick()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
