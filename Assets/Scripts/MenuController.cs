﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {
    //declare variables
    
    public Text highScoreText;

    public Button playButton;

	// Use this for initialization
	void Start () {
        //check if the Unity editor sees an object assigned
        if (highScoreText == null)
        {
            Debug.Log("Put a game object in high score text, please");
        }

        //print the high score from playerprefs
        highScoreText.GetComponent<Text>().text = "High Score: " + PlayerPrefs.GetInt("HIGH_SCORE", 0).ToString();
    }
	
	// Update is called once per frame
	void Update () {
        //clicks the button in editor and gamecontroller script takes care of the rest
        if (Input.GetKeyDown(KeyCode.Space))
        {
            playButton.onClick.Invoke();
        }
    }

    public void PlayButtonClick()
    {
        //load the actual game scene
        SceneManager.LoadScene("GameScene");
    }
}
