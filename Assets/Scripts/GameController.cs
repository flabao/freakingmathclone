﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class GameController : MonoBehaviour {
    //declare all these variables
    //text boxes
    public Text problemText;
    public Text answerText;
    public Text scoreText;
    
    //variables used for the timer bar
    public GameObject timeProgress;
    private float limitTime;
    private float currentTime;
    //public enum CalcToken { Add, Subtract } UNUSED

    //used for clicking the buttons
    public Button trueButton;
    public Button falseButton;

    //variables for the math
    private int operandLeft; 
    private int operandRight;    
    private int opMid;
    private int trueAnswer;
    private int shownAnswer;
    private int currentScore;

    /*change color of background
     * Currently hard coded 3 colours to be put into array at Start()
     */
    public Color[] c;
    public Color color1 = Color.red;
    public Color color2 = Color.blue;
    public Color color3 = Color.magenta;
    public float duration = 5.0f;
    public Camera cam;

    //used for the sound effects
    public AudioClip CorrectSound;
    public AudioClip WrongSound;
    public AudioSource MusicSource;

    //used for creating the math for the game
    public void CreateProblem()
    {
        //get random numbers
        operandLeft = Random.Range(0, 10);
        operandRight = Random.Range(0, 10);

        opMid = Random.Range(0, 3);

        //do the math for + (addition), - (subtraction), * (multiplication)
        switch (opMid)
        {
            case 0:
                trueAnswer = operandLeft + operandRight;
                shownAnswer = trueAnswer + Random.Range(-2, 2);
                problemText.GetComponent<Text>().text = operandLeft.ToString() + " + " + operandRight.ToString();
                answerText.GetComponent<Text>().text = shownAnswer.ToString();
                break;
            case 1:
                trueAnswer = operandLeft - operandRight;
                shownAnswer = trueAnswer - Random.Range(-3, 3);
                problemText.GetComponent<Text>().text = operandLeft.ToString() + " - " + operandRight.ToString();
                answerText.GetComponent<Text>().text = shownAnswer.ToString();
                break;
            case 2:
                trueAnswer = operandLeft * operandRight;
                shownAnswer = trueAnswer * Random.Range(-3, 3);
                problemText.GetComponent<Text>().text = operandLeft.ToString() + " x " + operandRight.ToString();
                answerText.GetComponent<Text>().text = shownAnswer.ToString();
                break;
            default:
                break;
        }

        //print the score
        scoreText.GetComponent<Text>().text = currentScore.ToString();
    }

    /*loads the game over scene and stores the score in Scores.current 
     * and the high score in PlayerPrefs
     This way, the high score stays even when user quits the game*/
    public void GameOver()
    {
        Scores.currentScore = currentScore;

        int highScore = PlayerPrefs.GetInt("HIGH_SCORE", 0);
        if(currentScore > highScore)
        {
            PlayerPrefs.SetInt("HIGH_SCORE", currentScore);
        }

        SceneManager.LoadScene("GameOverScene");
    }

    /* UNUSED Was supposed to be alternative to using case in CreateProblem() but was not working with multiplication
    public int pickOperator(int a, int b, CalcToken token)
    {
        if (token == CalcToken.Add) return a + b;
        if (token == CalcToken.Subtract) return a - b;
        throw new System.NotImplementedException();
    }
    */

    //when true button is clicked
    public void TrueButtonClick()
    {
        //adds to the score, makes a new problem, makes the timer reset, plays a successful sound
        if(trueAnswer == shownAnswer)
        {
            currentScore = ++currentScore;
            MusicSource.Play();
            CreateProblem();
            TimerReset();
            Debug.Log("CORRECT! " + currentScore);
        }
        //loads the game over screen after 1 failure
        else
        {
            MusicSource.clip = WrongSound;
            MusicSource.Play();
            Debug.Log("FALSE");
            GameOver();
        }
    }

    //similar to above, but for the false button
    public void FalseButtonClick()
    {
        if (trueAnswer != shownAnswer)
        {
            currentScore = ++currentScore;
            MusicSource.Play();
            CreateProblem();
            TimerReset();
            Debug.Log("CORRECT! " + currentScore);
        }
        else
        {
            MusicSource.clip = WrongSound;
            MusicSource.Play();
            Debug.Log("FALSE");
            GameOver();
        }
    }

    //takes a random number up to the array's length to pick the colour
    //background colour changes over the course of duration variable
    //only called at Start()
    public void ChangeColour()
    {
        int randomColour = Random.Range(0, c.Length);
        cam.backgroundColor = Color.Lerp(cam.backgroundColor, c[randomColour], duration);
    }

    //resets timer bar at top of scene
    public void TimerReset()
    {
        currentTime = 0.0f;
    }


    // Use this for initialization
    void Start () {

        c = new Color[3];
        c[0] = color1;
        c[1] = color2;
        c[2] = color3;

        ChangeColour();

        //check if the Unity editor sees an object assigned
        if (problemText == null)
        {
            Debug.Log("Put a game object in problem text, please");
        }
        if (answerText == null)
        {
            Debug.Log("Put a game object in answer text, please");
        }
        if (scoreText == null)
        {
            Debug.Log("Put a game object in score text, please");
        }
        if (timeProgress == null)
        {
            Debug.Log("Put a game object in time progress, please");
        }
        if (trueButton == null)
        {
            Debug.Log("Put a game object in true button, please");
        }
        if (falseButton == null)
        {
            Debug.Log("Put a game object in false button, please");
        }
        
        //5 second time limit for each round
        limitTime = 5.0f;
        currentTime = 0.0f;

        MusicSource.clip = CorrectSound;

        currentScore = 0;
        CreateProblem();
	}
	
	// Update is called once per frame
	void Update () {

        //left arrow click true button
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            trueButton.onClick.Invoke();
        }
        //right arrow clicks false button
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            falseButton.onClick.Invoke();
        }
        //escape key goes back to main menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MenuScene");
        }
        
        //when time runs out, call GameOver()
        currentTime += Time.deltaTime;
        if (currentTime > limitTime)
        {
            GameOver();
        }
        else
        {
            //change panel depending on time left
            float scale = 1.0f - currentTime / limitTime;
            timeProgress.transform.localScale = new Vector3(scale,1,1);
        }
	}

    
}
